<?php

/**
 * Adjusting webform elements according to multiple fieldset settings.
 *
 * @param  $form
 *   Form structure.
 * @param  $form_state
 *   Form state data.
 * @param  $submissions
 *   Tree-structure webform submissions.
 * @param  $tree
 *   Fieldsets component ids in the tree format.
 * @param  $data
 *   Additional parameters to pass through recursive function.
 *
 * @see  webform_multiset_form_alter().
 */
function webform_multiset_adjust_form(&$form, &$form_state, $submissions, $tree, $data = array()) {
  $data += array(
    'pid' => 0,
    'idx' => 0,
    'parents' => array(),
  );

  foreach ($tree as $cid => $children) {
    $component = $form['#node']->webform['components'][$cid];
    $component_parents = webform_component_parent_keys($form['#node'], $component);
    
    if ($data['pid'] != 0) {
      // $parents = array_diff($component_parents, array_filter($data['parents'], "_webform_multiset_remove_numeric"));
      // array_splice($parents, 1, 0, array($data['idx']));
      // $parents = array_merge($data['parents'], $parents);
      
      $parents = array_merge($data['parents'], array($component['form_key'], $data['idx']));
    }
    else {
      $parents = $component_parents;
    }
    
    $multiple_fieldset = &drupal_array_get_nested_value($form['submitted'], $parents);
    $fieldset_settings = $multiple_fieldset['#webform_component']['extra'];

    $wrapper_suffix = 'submitted-' . implode('-', $parents);
    
    if (empty($form_state[$wrapper_suffix]['num_names'])) {
      if ($submissions) {
        $sub_values = drupal_array_get_nested_value($submissions, $parents);
        if ($sub_values) {
          $sub_values = reset($sub_values);
          $num_names = count($sub_values);
        }
      }

      $form_state[$wrapper_suffix]['num_names'] = isset($num_names) ? $num_names : 1;
    }

    $multiple_fieldset['#prefix'] = '<div id="fieldset-wrapper--' . $wrapper_suffix . '">';
    $multiple_fieldset['#suffix'] = '</div>';

    if ($fieldset_settings['multiset']) {
      $multiple_fieldset['add_name'] = array(
        '#type' => 'submit',
        '#name' => 'add_name__' . $wrapper_suffix,
        '#value' => $fieldset_settings['multiset_settings']['add_button'],
        '#submit' => array('webform_multiset_add_more_add_one'),
        '#limit_validation_errors' => array(),
        '#attributes' => array('class' => array('add_more')),
        '#ajax' => array(
          'callback' => 'webform_multiset_add_more_callback',
          'wrapper' => 'fieldset-wrapper--' . $wrapper_suffix,
        ),
        '#weight' => 98,
      );

      if ($form_state[$wrapper_suffix]['num_names'] > 1) {
        $multiple_fieldset['remove_name'] = array(
          '#type' => 'submit',
          '#name' => 'remove_name__' . $wrapper_suffix,
          '#value' => $fieldset_settings['multiset_settings']['remove_button'],
          '#submit' => array('webform_multiset_add_more_remove_one'),
          '#limit_validation_errors' => array(),
          '#attributes' => array('class' => array('remove_one')),
          '#ajax' => array(
            'callback' => 'webform_multiset_add_more_callback',
            'wrapper' => 'fieldset-wrapper--' . $wrapper_suffix,
          ),
          '#weight' => 99,
        );
      }
    }

    foreach (element_children($multiple_fieldset) as $element) {
      if (in_array($element, array('add_name', 'remove_name'))) {
        continue;
      }

      $field = $multiple_fieldset[$element];
      $multiple_fieldset[$element] = array();
      for ($i = $form_state[$wrapper_suffix]['num_names']; $i > 0; $i--) {
        if ($submissions) {
          $field['#default_value'] = drupal_array_get_nested_value($submissions, array_merge($parents, array($element, $i - 1)));
        }
        $field['#weight'] = $i - 1;
        $multiple_fieldset[$element][$i-1] = $field;
      }
    }

    for ($i = 0; $i < $form_state[$wrapper_suffix]['num_names']; $i++) {
      if ($children) {
        // $idx_arr[$cid] = $i;
        $data['idx'] = $i;
        $data['pid'] = $cid;
        $data['parents'] = $parents;
        webform_multiset_adjust_form($form, $form_state, $submissions, $children, $data);
      }
    }
  }
}

/**
 * Adjusting form state values according to multiple fieldset settings.
 *
 * @param  $result
 *   Referenced var that filled by new form state values structure.
 * @param  $form
 *   Form structure.
 * @param  $form_state
 *   Form state data.
 * @param  $tree
 *   Fieldsets component ids in the tree format.
 * @param  $data
 *   Additional parameters to pass through recursive function.
 *
 * @see  webform_multiset_adjust_form_submit().
 */
function webform_multiset_adjust_form_submit_alter(&$result, $form, $form_state, $tree, $data = array()) {
  $data += array(
    'pid' => 0,
    'idx' => 0,
    'parents' => array(),
  );

  foreach ($tree as $cid => $children) {
    $component = $form['#node']->webform['components'][$cid];
    $component_parents = webform_component_parent_keys($form['#node'], $component);
    
    if ($data['pid'] != 0) {
      $parents = array_merge($data['parents'], array($component['form_key'], $data['idx']));
    }
    else {
      $parents = $component_parents;
    }

    $values = &drupal_array_get_nested_value($form_state['values']['submitted'], $parents);
    $multiple_fieldset = drupal_array_get_nested_value($form['submitted'], $parents);

    foreach ($values as $element => $items) {
      if (in_array($element, array('add_name', 'remove_name'))) {
        continue;
      }

      if (is_array($items) && count($items)) {
        foreach ($items as $i => $item) {
          $is_value = FALSE;
          $to_merge = array($element);

          switch ($multiple_fieldset[$element][$i]['#type']) {
            case 'managed_file':
              $to_merge = array_merge($to_merge, array('fid'));
              if (isset($t)) {
                $result_item = &$result_item['fid'];
              }
              break;
            case 'date':
            case 'select':
            case 'checkboxes':
            case 'webform_time':
            case 'webform_grid':
              $is_value = TRUE;

              break;
          }

          if (is_array($item) && !$is_value) {
            $data['idx'] = $i;
            $data['pid'] = $cid;
            $data['parents'] = $parents;
            webform_multiset_adjust_form_submit_alter($result, $form, $form_state, $children, $data);
          }
          else {
            $key = implode('][', array_merge($parents, array($element, $i)));
            
            $result_parents = array_merge($component_parents, $to_merge);
            
            $result_item = &drupal_array_get_nested_value($result, $result_parents);

            if (isset($result_item)) {
              $result_item[$key] = $item;
            }
            else {
              drupal_array_set_nested_value($result, $result_parents, array($key => $item), TRUE);
            }
          }
        }
      }
    }
  }
}

/**
 * Adjusting renderable array according to multiple fieldset settings.
 *
 * @param  $renderable
 *   Renderable array structure.
 * @param  $submissions
 *   Tree-structure webform submissions.
 * @param  $tree
 *   Fieldsets component ids in the tree format.
 * @param  $data
 *   Additional parameters to pass through recursive function.
 *
 * @see  webform_multiset_webform_submission_render_alter().
 */
function webform_multiset_adjust_renderable(&$renderable, $submissions, $tree, $data = array()) {
  $data += array(
    'pid' => 0,
    'idx' => 0,
    'parents' => array(),
  );

  foreach ($tree as $cid => $children) {
    $component = $renderable['#node']->webform['components'][$cid];
    $component_parents = webform_component_parent_keys($renderable['#node'], $component);
    
    if ($data['pid'] != 0) {
      $parents = array_merge($data['parents'], array($component['form_key'], $data['idx']));
    }
    else {
      $parents = $component_parents;
    }

    $multiple_fieldset = &drupal_array_get_nested_value($renderable, $parents);
    $sub = reset(drupal_array_get_nested_value($submissions, $parents));
    $fieldset_count = count($sub);
    
    foreach (element_children($multiple_fieldset) as $element) {
      $field = $multiple_fieldset[$element];
      $multiple_fieldset[$element] = array();
      for ($i = $fieldset_count; $i > 0; $i--) {
        if (!isset($field['#type']) || $field['#type'] != 'fieldset') {
          $field['#value'] = drupal_array_get_nested_value($submissions, array_merge($parents, array($element, $i - 1)));
        }
        $multiple_fieldset[$element][$i-1] = $field;
      }
    }

    for ($i = 0; $i < $fieldset_count; $i++) {
      if ($children) {
        $data['idx'] = $i;
        $data['pid'] = $cid;
        $data['parents'] = $parents;
        webform_multiset_adjust_renderable($renderable, $submissions, $children, $data);
      }
    }

  }
}
